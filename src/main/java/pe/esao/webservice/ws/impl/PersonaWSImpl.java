package pe.esao.webservice.ws.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.xml.ws.BindingType;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.soap.SOAPBinding;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import pe.esao.webservice.entity.Persona;
import pe.esao.webservice.entity.ws.types.BuscarRequest;
import pe.esao.webservice.entity.ws.types.BuscarResponse;
import pe.esao.webservice.entity.ws.types.ListaPersonaType;
import pe.esao.webservice.entity.ws.types.PersonaType;
import pe.esao.webservice.services.IPersonaService;
import pe.esao.webservice.services.PersonaServiceImpl;
import pe.esao.webservice.services.personaws.ws.PersonaWS;


@WebService(
		serviceName = "personaWS", 
		targetNamespace = "http://webservice.esao.pe/services/personaws/ws", 
		portName = "personaWS", 
		endpointInterface = "pe.esao.webservice.services.personaws.ws.PersonaWS", 
		wsdlLocation = "META-INF/wsdl/personaWS.wsdl")
@BindingType(SOAPBinding.SOAP11HTTP_BINDING)
public class PersonaWSImpl implements PersonaWS{
	
	private static final Logger logger = Logger.getLogger(PersonaWSImpl.class.getName());
	
	@Resource
	private WebServiceContext context;

	@Override
	public BuscarResponse buscarDatos(BuscarRequest request) {
		logger.info("[buscarDatos] =========== INICIO ===========");
		BuscarResponse response = new BuscarResponse();
		try {
			ServletContext servletContext = (ServletContext) context.getMessageContext().get("javax.xml.ws.servlet.context");
			WebApplicationContext webApplicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
			
			IPersonaService personaService = (PersonaServiceImpl) webApplicationContext.getAutowireCapableBeanFactory().getBean("personaServiceImpl");
			
			List<Persona> lta = personaService.find(request.getId());
			
			if(lta != null && lta.size() > 0) {
				ListaPersonaType ltaType = new ListaPersonaType();
				
				for(Persona persona : lta) {
					PersonaType per = new PersonaType();
					per.setId(persona.getId());
					per.setIdentificacion(persona.getIdentificacion());
					per.setNombres(persona.getNombres());
					per.setApellidos(persona.getApellidos());
					per.setEdad(persona.getEdad());
					ltaType.getPersona().add(per);
				}
				
				response.setListaPersona(ltaType);
			}
		}catch(Exception e) {
			logger.error("Exception: ", e);
		}
		return response;
		
	}
}
