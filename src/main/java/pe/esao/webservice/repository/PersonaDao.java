package pe.esao.webservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.esao.webservice.entity.Persona;

@Repository
public interface PersonaDao extends JpaRepository<Persona, Integer>  {

}
