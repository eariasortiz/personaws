package pe.esao.webservice.services;

import java.util.List;

import pe.esao.webservice.entity.Persona;

public interface IPersonaService {

	List<Persona> find(Integer id);
}
