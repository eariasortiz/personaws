package pe.esao.webservice.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.esao.webservice.entity.Persona;
import pe.esao.webservice.repository.PersonaDao;


@Service
public class PersonaServiceImpl implements IPersonaService {

	private static final Logger logger = Logger.getLogger(PersonaServiceImpl.class.getName());

	@Autowired
	private PersonaDao personaDao;
	
	@Override
	public List<Persona> find(Integer id) {
		List<Persona> ltaPersona = null;
		if (id != null) {
			ltaPersona = new ArrayList<Persona>();
			ltaPersona.add(personaDao.findOne(id));
		} else {
			ltaPersona = personaDao.findAll();
		}
		return ltaPersona;
	}

}
