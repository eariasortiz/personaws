package pe.esao.webservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pe.esao.webservice.entity.Persona;
import pe.esao.webservice.services.IPersonaService;

@Controller
@RequestMapping("status")
public class StatusController {

	@Autowired
	private IPersonaService personaService;

	
	@RequestMapping(value="persona", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<Persona>> findCategorias() {
		return new ResponseEntity<List<Persona>>(personaService.find(null), HttpStatus.OK);
	}
		
	
	
}
