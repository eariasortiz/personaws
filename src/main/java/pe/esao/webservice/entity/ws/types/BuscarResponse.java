
package pe.esao.webservice.entity.ws.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="listaPersona" type="{http://webservice.esao.pe/entity/ws/types}ListaPersonaType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "listaPersona"
})
@XmlRootElement(name = "buscarResponse")
public class BuscarResponse {

    @XmlElement(required = true)
    protected ListaPersonaType listaPersona;

    /**
     * Obtiene el valor de la propiedad listaPersona.
     * 
     * @return
     *     possible object is
     *     {@link ListaPersonaType }
     *     
     */
    public ListaPersonaType getListaPersona() {
        return listaPersona;
    }

    /**
     * Define el valor de la propiedad listaPersona.
     * 
     * @param value
     *     allowed object is
     *     {@link ListaPersonaType }
     *     
     */
    public void setListaPersona(ListaPersonaType value) {
        this.listaPersona = value;
    }

}
